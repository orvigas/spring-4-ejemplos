package concursantes;

import org.springframework.stereotype.Component;

@Component
public class Voluntario implements Pensador {

	private String pensamientos;

	@Override
	public void pensarEnAlgo(String pensamientos) {
		System.out.println("Ejecutando");
		this.pensamientos = pensamientos;
	}

	public String getPensamiento() {
		return this.pensamientos;
	}

}
