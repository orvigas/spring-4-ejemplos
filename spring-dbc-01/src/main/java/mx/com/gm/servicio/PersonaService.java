package mx.com.gm.servicio;

import java.util.List;

import mx.com.gm.jdbc.Persona;

public interface PersonaService {

	public List<Persona> listarPersonas();

	public Persona recuperarPersona(Persona persona);

	public void agregaPersona(Persona persona);

	public void modificaPersona(Persona persona);

	public void eliminaPersona(Persona pesona);

}
