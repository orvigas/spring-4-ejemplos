package test;

import static org.junit.Assert.*;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import mx.com.gm.jdbc.Persona;
import mx.com.gm.jdbc.PersonaDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:datasource-test.xml", "classpath:ApplicationContext.xml" })
public class TestPersonaDaoImpl {

	private static Log logger = LogFactory.getLog("TestJdbc");

	@Autowired
	private PersonaDao personaDao;

	@Test
	@Ignore // Omitir la ejecución del método
	public void deberiaMostrarPersonas() {
		try {
			System.out.println("");
			logger.info("Inicio del test deberiaMostrarPersonas");

			List<Persona> personas = personaDao.findAllPersonas();
			int contadorPersonas = 0;
			for (Persona persona : personas) {
				logger.info("Persona: " + persona);
				contadorPersonas++;
			}

			assertEquals(contadorPersonas, personaDao.contadorPersonas());

			logger.info("Fin del test deberiaMostrarPersonas");
		} catch (Exception e) {
			logger.error("Error JDBC: " + e);
		}
	}

	@Test
	@Ignore
	public void testContarPersonasPorNombre() {
		try {
			System.out.println("");
			logger.info("Inicio del test testContarPersonasPorNombre");
			String nombre = "Juan";
			Persona persona = new Persona();
			persona.setNombre(nombre);

			int coincidencias = personaDao.contadorPersonasPorNombre(persona);
			logger.info("Número de personas encontradas por el nombre: '" + nombre + "' " + coincidencias);

			assertEquals(2, coincidencias);

			logger.info("Fin del test testContarPersonasPorNombre");
		} catch (Exception e) {
			logger.error("Error JDBC: " + e);
		}
	}

	@Test
	@Ignore
	public void deberiaEncontrarPersonaPorId() {
		try {
			System.out.println("");
			logger.info("Inicio del test deberiaEncontrarPersonaPorId");
			Long idPersona = (long) 1;
			Persona persona = personaDao.findPersonaById(idPersona);
			logger.info("Persona recuperada (id=" + idPersona + "): " + persona);
			assertEquals("Mateo Armando", persona.getNombre());

			logger.info("Fin del test deberiaEncontrarPersonaPorId");
		} catch (Exception e) {
			logger.error("Error JDBC: " + e);
		}
	}

	@Test
	@Ignore
	public void deberiaInsertarPersona() {
		try {
			System.out.println("");
			logger.info("Inicio del test deberia insertarPersona");
			assertEquals(5, personaDao.contadorPersonas());
			Persona persona = new Persona();
			persona.setApeMaterno("Robles");
			persona.setApePaterno("Almanza");
			persona.setNombre("Jaqueline");
			persona.setEmail("jaque@gmail.com");

			personaDao.insertPersona(persona);

			persona = personaDao.getPersonaByEmail(persona);
			logger.info("Persona insertada " + persona);

			assertEquals(6, personaDao.contadorPersonas());
			logger.info("Fin test deberiaInsertarPersona");
		} catch (Exception e) {
			logger.error("Error JDBC: " + e);
		}

	}

	@Test
	@Ignore
	public void deberiaActualizarPersona() {
		System.out.println("");
		logger.info("inicio del test deberiaInsertarPersona");
		Long id = (long) 3;
		Persona persona = personaDao.findPersonaById(id);
		logger.info("Persona a modificar (id=" + id + ") " + persona);

		persona.setNombre("Ernesto");
		persona.setApePaterno("Esteban");

		personaDao.updatePersona(persona);

		persona = personaDao.findPersonaById(id);

		assertEquals("Ernesto", persona.getNombre());
		logger.info("Persona a modificada (id=" + id + ") " + persona);

		logger.info("Fin test deberiaActualizarPersona");
		try {
		} catch (Exception e) {
			logger.error("Error JDBC: " + e);
		}

	}

	@Test
	public void deberiaEliminarPersona() {
		System.out.println("");
		logger.info("inicio del test deberiaEliminarPersona");
		Long id = (long) 3;
		Persona persona = personaDao.findPersonaById(id);
		logger.info("Persona a eliminar (id=" + id + ") " + persona);
		personaDao.deletePersona(persona);
		persona = personaDao.findPersonaById(id);
		
		assertNull(persona);

		List<Persona> personas = personaDao.findAllPersonas();

		int contadorPersona = 0;

		for (Persona item : personas) {
			logger.info("persona: " + item);
			contadorPersona++;
		}

		assertEquals(contadorPersona, personaDao.contadorPersonas());

		logger.info("Fin test deberiaEliminarPersona");
		try {
		} catch (Exception e) {
			logger.error("Error JDBC: " + e);
		}

	}

}
