package test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import mx.com.gm.jdbc.Persona;
import mx.com.gm.jdbc.PersonaDao;
import mx.com.gm.servicio.PersonaService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:datasource-test.xml", "classpath:ApplicationContext.xml" })
public class TestPersonaServiceImpl {

	private static Log logger = LogFactory.getLog("TestPersonaServiceImpl");
	@Autowired
	private PersonaService personaService;
	@Autowired
	private PersonaDao personaDao;

	@Test
	@Transactional
	public void deberiaMostrarPersonas() {
		try {
			System.out.println("");
			logger.info("Inicio del test debería mostrar personas");
			int contadorPersonas = this.desplegarPersonas();
			assertEquals(contadorPersonas, personaDao.contadorPersonas());
			logger.info("Fin del test debería mostrar personas");
			System.out.println("");
		} catch (Exception e) {
			logger.error("Error de servicio: " + e);
		}
	}

	private int desplegarPersonas() {
		List<Persona> personas = personaService.listarPersonas();
		int contadorPersonas = 0;
		for (Persona persona : personas) {
			logger.info("Persona: " + persona);
			contadorPersonas++;
		}
		return contadorPersonas;
	}

	@Test
	@Transactional
	public void testOperaciones() {
		try {
			System.out.println("");
			logger.info("Inicio del test operaciones");

			Persona persona1 = new Persona();
			//persona1.setNombre("Ángel****************************************************");
			persona1.setNombre("Ángel");
			persona1.setApePaterno("Romero");
			persona1.setApeMaterno("Flores");
			persona1.setEmail("angel@gmail.com");

			personaService.agregaPersona(persona1);

			assertEquals(6, personaDao.contadorPersonas());
			Persona personAux = new Persona();
			personAux.setIdPersona(1);
			Persona persona2 = personaService.recuperarPersona(personAux);
			persona2.setNombre("Ritchie");

			personaService.modificaPersona(persona2);

			this.desplegarPersonas();

			logger.info("Fin del test operaciones");
			System.out.println("");
		} catch (Exception e) {
			logger.error("Error de servicio: " + e);
		}
	}

	@Test
	public void testCompruebaOperaciones() {
		try {
			System.out.println("");
			logger.info("Inicio del test operaciones");

			assertEquals(5, personaDao.contadorPersonas());
			this.desplegarPersonas();

			logger.info("fin del test operaciones");
			System.out.println("");
		} catch (Exception e) {
			logger.error("Error de servicio: " + e);
		}
	}

}
