package mx.com.gm.capaweb;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import mx.com.gm.capadatos.domain.Persona;
import mx.com.gm.capaservicio.PersonaService;

public class PersonaAction extends DispatchAction {
	private PersonaService personaService;

	public void setPersonaService(PersonaService personaService) {
		this.personaService = personaService;
	}

	private static Log logger = LogFactory.getLog("PersonaAction");

	public ActionForward actionListar(ActionMapping mapping, ActionForm form, HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		logger.info("Ejecutando método listar");
		this.setListarPersonas(req);
		return mapping.findForward("listar");
	}

	private void setListarPersonas(HttpServletRequest req) {
		List<Persona> personas = personaService.listarPersonas();
		req.setAttribute("personas", personas);
		for (Persona persona : personas) {
			logger.info("Persona: " + persona);
		}
	}

}
