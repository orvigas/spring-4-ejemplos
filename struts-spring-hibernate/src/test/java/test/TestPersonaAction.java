package test;

import servletunit.struts.MockStrutsTestCase;

public class TestPersonaAction extends MockStrutsTestCase {

	public void testListar() {
		System.out.println("");
		logger.info("Iniciando el test listar con struts");
		setRequestPathInfo("/inicio");
		addRequestParameter("metodo", "accionListar");
		actionPerform();
		verifyForward("listar");
		logger.info("Se redirecciono exitosamente");
		verifyNoActionErrors();
		logger.info("Termino el test listar con struts");
		System.out.println("");
	}

}
