package test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import mx.com.gm.capadatos.PersonaDao;
import mx.com.gm.capadatos.domain.Persona;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:datasource-test.xml", "classpath:ApplicationContext.xml" })
public class TestPersonaDaoImpl {

	private static Log logger = LogFactory.getLog("TestPersonaDaoImpl");
	@Autowired
	private PersonaDao personaDao;

	@Test
	@Transactional
	public void deberiaMostrarPersonas() {
		try {
			System.out.println("");
			logger.info("Inicio del test debería mostrar personas");
			List<Persona> personas = personaDao.findAllPersonas();
			int contadorPersonas = 0;
			for (Persona persona : personas) {
				logger.info("Persona: " + persona);
				contadorPersonas++;
			}
			assertEquals(contadorPersonas, personaDao.contadorPersonas());
			logger.info("Fin del test debería mostrar personas");
			System.out.println("");
		} catch (Exception e) {
			logger.error("Error de servicio: " + e);
		}
	}

	@Test
	@Transactional
	public void deberiaEncontrarPersonaPorId() {
		try {
			System.out.println("");
			logger.info("Inicio del test deberiaEncontrarPersonaPorId");
			Long idPersona = (long) 1;
			Persona persona = personaDao.findPersonaById(idPersona);
			assertEquals("Mateo Armando", persona.getNombre());
			logger.info("persona recuperada: id=" + idPersona + " " + persona);
			logger.info("Fin del test deberiaEncontrarPersonaPorId");
			System.out.println("");
		} catch (Exception e) {
			logger.error("Error de servicio: " + e);
		}
	}

	@Test
	@Transactional
	public void deberiaInsertarPersona() {
		try {
			System.out.println("");
			logger.info("Inicio del test deberiaInsertarPersona");
			assertEquals(5, personaDao.contadorPersonas());
			Persona persona = new Persona();
			persona.setNombre("Fulano");
			persona.setApePaterno("De");
			persona.setApeMaterno("Tal");
			persona.setEmail("fulano@detal.com");
			
			personaDao.insertPersona(persona);
			
			persona = personaDao.getPersonaByEmail(persona);
			
			assertEquals(6, personaDao.contadorPersonas());
			
			this.deberiaMostrarPersonas();
			
			logger.info("Fin del test deberiaInsertarPersona");
			System.out.println("");
		} catch (Exception e) {
			logger.error("Error de servicio: " + e);
		}
	}

}
